# flood-it

This is the solution for flood-it game.

### Dependency install
```
pip install -r requirement.txt
```

### Execute
```
python flood.py "board_image"
eg. python flood.py IMG_1064.jpg
```
