import sys
from board import board
from solve import solve

if __name__ == '__main__':
    '''
    main for setup board and solve board
    '''
    board = board()
    matrix, graph, col_val = board.make_board(sys.argv[1])
    slv = solve(matrix, graph)
    solution = slv.start((0,0))
    print("--------COLOR VALUES----------")
    print(col_val)
    print("------------------------------")
    print("NUMBER OF STEPS", ":", len(solution))
    print("COLOR SEQUENCE", ":", solution)
