class solve():

    def __init__(self, matrix, graph):
        self.matrix = matrix
        self.graph = graph
        self.del_node = set()

    def get_matrix_val(self, val):
        '''
        get value of index on board matrix
        '''
        try:
            if val[0] < 0 or val[1] < 0:
                return None
            return self.matrix[val[0]][val[1]]
        except Exception:
            return None

    def score_right(self, x, y, cost=None):
        '''
        get score for the right child
        '''
        if cost is None:
            cost = 0
        value = self.get_matrix_val((x, y))
        x = x + 1
        if self.get_matrix_val((x, y)) != value:
            return cost
        cost += 10
        return self.score_right(x, y, cost=cost)

    def score_left(self, x, y, cost=None):
        '''
        get score for left child
        '''
        if cost is None:
            cost = 0
        value = self.get_matrix_val((x, y))
        y = y + 1
        if self.get_matrix_val((x, y)) != value:
            return cost
        cost += 10
        return self.score_left(x, y, cost=cost)

    def get_score(self, x, y):
        '''
        get score for the index
        '''
        return sum([self.score_right(x, y), self.score_left(x, y)])

    def get_next(self, childs):
        '''
        return the index value of matrix board selected as a color for next move
        '''
        cost_dic = {}
        for obj in childs:
            cost_dic[obj] = self.get_score(obj[0], obj[1])

        if len(set(cost_dic.values())) == 1:
            col_val = {}
            for obj in childs:
                try:
                    col_val[self.get_matrix_val(obj)].append(obj)
                except KeyError:
                    n_list = []
                    n_list.append(obj)
                    col_val[self.get_matrix_val(obj)] = n_list
            return (max(col_val.values(), key=lambda x: len(x)))[0]
        return max(cost_dic.items(), key=lambda x: x[1])[0]

    def parent_child(self, node):
        '''
        return the set of a parent node/index of a node/index
        '''
        setpc = set()
        if self.get_matrix_val((node[0]-1, node[1])) is not None:
            setpc.add((node[0]-1, node[1]))
            setpc = setpc.union(self.graph[(node[0]-1, node[1])])
        if self.get_matrix_val((node[0], node[1]-1)) is not None:
            setpc.add((node[0], node[1]-1))
            setpc = setpc.union(self.graph[(node[0], node[1]-1)])

        return setpc - self.del_node

    def filter_childs(self, childs, parent):
        '''
        removes the process nodes
        '''
        temp = set()
        for obj in childs:
            if self.get_matrix_val(parent) == self.get_matrix_val(obj):
                temp = temp.union(self.graph[obj])
                temp = temp.union(self.parent_child(obj))
                self.del_node.add(obj)
        if temp:
            cousins = temp - self.del_node
            result = self.filter_childs(cousins, parent)
            childs = childs.union(result)
        return childs - self.del_node

    def mark(self, d, val):
        for obj in d:
            self.matrix[obj[0]][obj[1]] = val
        return self.matrix

    def start(self, node, childs=None, steps=None):
        '''
        main function to start solve
        '''
        if childs is None:
            childs = set()
        if steps is None:
            steps = []
        self.del_node.add(node)
        childs = self.filter_childs(childs.union(self.graph[node]), node)
        print(node)
        mat = self.mark(self.del_node, self.get_matrix_val(node))
        for obj in mat:
            print(obj)
        if childs:
            g_next = self.get_next(childs)
            steps.append(self.get_matrix_val(g_next))
            if len(self.del_node) == len(self.graph.keys()):
                return False
            self.start(g_next, childs=childs, steps=steps)
        return steps
